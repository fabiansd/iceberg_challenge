#Load data
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD,Adam
from keras import optimizers
import pickle

from keras.layers.normalization import BatchNormalization
from keras.applications.xception import Xception
from keras.applications.mobilenet import MobileNet
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from resnet import ResnetBuilder

from keras.utils import to_categorical
from keras.callbacks import EarlyStopping, ReduceLROnPlateau


import os

os.environ['CUDA_VISIBLE_DEVICES'] = '2'

train_df = pd.read_json(os.path.join('..','..','data','train.json'))
test_df = pd.read_json(os.path.join('..','..','data','test.json'))


# Load training data
X_band1_train = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in train_df["band_1"]])
X_band2_train = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in train_df["band_2"]])
X_band3_train = (X_band1_train+X_band2_train)/2 

X_angle_train = np.array(train_df.inc_angle)
Y_train = np.array(train_df["is_iceberg"])




# Load testing data
X_band1_test = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in test_df["band_1"]])
X_band2_test = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in test_df["band_2"]])
X_band3_test = (X_band1_test-X_band2_test)
X_angle_test = np.array(test_df.inc_angle)



#No missing angles
test_df.inc_angle = test_df.inc_angle.replace('na', float('nan'))
test_df = test_df.dropna()



#Normalize channels
from keras.preprocessing.image import ImageDataGenerator

normalize_all = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization=True
)

normalize1 = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization = True
)
normalize2 = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization = True
)
normalize3 = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization = True
)

#Angles normalized to be between 0 and 1
X_angle_train = (X_angle_train - min_inc_angle) / (max_inc_angle - min_inc_angle)
X_angle_test = (X_angle_test - min_inc_angle) / (max_inc_angle - min_inc_angle)

X_train_all = np.concatenate([X_band1_train,X_band2_train,X_band3_train],axis=-1)

normalize_all.fit(X_train_all)

X_train = next(normalize_all.flow(X_train_all,None,batch_size=X_band1_train.shape[0],shuffle=False))
'''
normalize1.fit(X_band1_train)
normalize2.fit(X_band2_train)
normalize3.fit(X_band3_train)


X_band1_train_n = next(normalize1.flow(X_band1_train,None,batch_size=X_band1_train.shape[0],shuffle=False))
X_band2_train_n = next(normalize1.flow(X_band2_train,None,batch_size=X_band2_train.shape[0],shuffle=False))
X_band3_train_n = next(normalize1.flow(X_band3_train,None,batch_size=X_band3_train.shape[0],shuffle=False))
X_train = np.concatenate([X_band1_train_n,X_band2_train_n,X_band3_train_n],axis=-1)

X_band1_test_n = next(normalize1.flow(X_band1_test,None,batch_size=X_band1_test.shape[0],shuffle=False))
X_band2_test_n = next(normalize1.flow(X_band2_test,None,batch_size=X_band2_test.shape[0],shuffle=False))
X_band3_test_n = next(normalize1.flow(X_band3_test,None,batch_size=X_band3_test.shape[0],shuffle=False))
X_test = np.concatenate([X_band1_test_n,X_band2_test_n,X_band3_test_n],axis=-1)
'''
print('training shapes')
print(X_train.shape)
print(X_angle_train.shape)
print(Y_train.shape)

#print('Test shapes')
#print(X_test.shape)
#print(X_angle_test.shape)


<<<<<<< HEAD:resCNN/main.py
model = ResnetBuilder.build_resnet_101((75,75,3),1)
=======
model = ResnetBuilder.build_resnet_152([(75,75,3),[1]],1)
#model = ResnetBuilder.build_resnet_34((75,75,3),1)

<<<<<<< HEAD
model = ResnetBuilder.build_resnet_152((75,75,3),1)
=======
>>>>>>> 1813bfb15c8766935b7848bcca1068ab78294732
>>>>>>> test_resnet:resCNN/adrian/main.py
model.compile(loss='binary_crossentropy',
              optimizer='adagrad',
              metrics=['accuracy'])
print(model.summary())

augmentation = ImageDataGenerator(
<<<<<<< HEAD
    rotation_range = 15,
<<<<<<< HEAD:resCNN/main.py
    width_shift_range = 0.2,
    height_shift_range = 0.2,
    zoom_range=0.05,
=======
    width_shift_range = 0.3,
    height_shift_range = 0.3,
    zoom_range=0.05,
=======
    rotation_range = 45,
    width_shift_range = 0.1,
    height_shift_range = 0.1,
    zoom_range=0.1,
>>>>>>> 1813bfb15c8766935b7848bcca1068ab78294732
>>>>>>> test_resnet:resCNN/adrian/main.py
    vertical_flip=True,
    horizontal_flip=True
)


X_train_s, X_valid_s, X_angle_train_s, X_angle_valid_s, Y_train_s, Y_valid_s = train_test_split(X_train, X_angle_train , Y_train, test_size=0.15, random_state=42, stratify=Y_train)
print('Data split shapes')
print(X_train_s.shape)
print(X_angle_train_s.shape)
print(Y_train_s.shape)

print(X_valid_s.shape)
print(X_angle_valid_s.shape)
print(Y_valid_s.shape)

<<<<<<< HEAD:resCNN/main.py
early_stopping = EarlyStopping(monitor='val_loss', patience=10)
=======
early_stopping = EarlyStopping(monitor='val_loss', patience=20)
>>>>>>> test_resnet:resCNN/adrian/main.py
checkpoint = keras.callbacks.ModelCheckpoint('checkpoint_resnet.h5', monitor='val_acc', verbose=1, save_best_only=True, mode='max')
lr_adjust = ReduceLROnPlateau(monitor='val_acc',mode='max')

batch_size = 25

def createGenerator( X, I, Y,batch_size):

    if X.shape[0] % batch_size: print('Warning: datasize not divisible with batch_size')
    np.random.seed(seed=10)
    while True:
  
        
        idx = np.random.permutation( X.shape[0])
        #idx = np.arange(X.shape[0])
        augmentation = ImageDataGenerator(
            rotation_range = 45,
            width_shift_range = 0.2,
            height_shift_range = 0.2,
            zoom_range=0.1,
            vertical_flip=True,
            horizontal_flip=True
        )

<<<<<<< HEAD:resCNN/main.py
#Run model
history = model.fit_generator(augmentation.flow(X_train_s, Y_train_s),
                    steps_per_epoch=42, epochs=100, 
=======

        idx0 = 0
        for batch in augmentation.flow( X[idx], Y[idx], batch_size=batch_size, shuffle=False):

            idx1 = idx0 + batch[0].shape[0]
            yield [batch[0], I[ idx[ idx0:idx1 ] ]], batch[1]
            #yield [batch[0], batch[1]]

            idx0 = idx1
            if idx1 >= X.shape[0]:
                break


history = model.fit_generator(createGenerator(X_train_s,X_angle_train_s,Y_train_s,batch_size=batch_size),
                    steps_per_epoch=X_train_s.shape[0]/batch_size, 
                    epochs=50, 
>>>>>>> test_resnet:resCNN/adrian/main.py
                    callbacks=[early_stopping,checkpoint],
                    #validation_data=(X_valid_s,Y_valid_s))
                    validation_data=([X_valid_s,X_angle_valid_s],Y_valid_s))


#history = model.fit_generator(augmentation.flow(X_train_s,Y_train_s,seed=10,batch_size=50),
#                    steps_per_epoch=X_train_s.shape[0]/batch_size, epochs=10, 
#                    callbacks=[early_stopping],
#                    validation_data=(X_valid_s,Y_valid_s))

model.save('saved_resnet_adrian.h5')
print(history)
with open('results.pickle', 'wb') as f:
    pickle.dump(history.history, f)


model.save('model.h5')
<<<<<<< HEAD:resCNN/main.py
'''
exit(0)
=======


>>>>>>> test_resnet:resCNN/adrian/main.py

epochs = 10
batch_size = 40
n = len(X_train_s)
'''
history = []
for i in range(epochs):
    bn=1

            np.random.seed(seed=i)
            np.random.shuffle(X_train_s)
            np.random.seed(seed=i)
            np.random.shuffle(Y_train_s)
            np.random.seed(seed=i)
            np.random.shuffle(X_angle_train)



    for x_batch, y_batch in augmentation.flow(X_train_s, Y_train_s,batch_size=batch_size):
        inc_angle_batch = X_angle_train[(bn-1)*batch_size:bn*batch_size]
        temp_history = model.train_on_batch(x_batch,y_batch)
        history.append(temp_history)
        print(temp_history)
        if bn >= n / batch_size:
            print('Epoch: ' + str(i))
            break
        bn += 1


'''
