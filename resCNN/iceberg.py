import pandas as pd
import numpy as np
import skimage

import matplotlib.pyplot as plt

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model
from keras.layers import *
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.regularizers import l2
from keras.models import Model, load_model
from keras import optimizers

from sklearn import preprocessing
from sklearn.metrics import accuracy_score


from keras import optimizers
from keras.callbacks import EarlyStopping

from keras import backend as k

import _pickle as pickle
import os


class CLassifier:

	def __init__(self,train_path=os.path.join('..','data','train.json'),test_path=os.path.join('..','data','test.json'),batch_size=40,pretrained_model=None):


		self.train_df = pd.read_json(train_path)
		self.test_df = pd.read_json(test_path)



		# Train data
		x_band1 = np.array([np.array(band).astype(np.float32).reshape(75, 75) for band in self.train_df["band_1"]])
		x_band2 = np.array([np.array(band).astype(np.float32).reshape(75, 75) for band in self.train_df["band_2"]])
		self.X_train = np.concatenate([x_band1[:, :, :, np.newaxis], x_band2[:, :, :, np.newaxis], ((x_band1+x_band2)/2)[:, :, :, np.newaxis]], axis=-1)
		self.y_train = np.array(self.train_df["is_iceberg"]).astype('int')




		self.train_df.inc_angle = self.train_df.inc_angle.replace('na', 0)
		self.train_df.inc_angle = self.train_df.inc_angle.astype(float).fillna(0.0)

		self.X_angle_train = np.array(self.train_df.inc_angle)

		print()


		print("Xtrain shape:", self.X_train.shape)




#		x_band1 = np.array([np.array(band).astype(np.float32).reshape(75, 75) for band in self.test_df["band_1"]])
#		x_band2 = np.array([np.array(band).astype(np.float32).reshape(75, 75) for band in self.test_df["band_2"]])
#		self.X_test = np.concatenate([x_band1[:, :, :, np.newaxis], x_band2[:, :, :, np.newaxis], ((x_band1+x_band2)/2)[:, :, :, np.newaxis]], axis=-1)
#		self.test_df.inc_angle = self.test_df.inc_angle.replace('na', 0)
#		self.test_df.inc_angle = self.test_df.inc_angle.astype(float).fillna(0.0)
#		self.X_angle_test = np.array(self.test_df.inc_angle)
#		print("Xtest shape:", self.X_test.shape)


		#Network parameters
		self.stride=1
		self.alpha = 0.2
		self.dropout = 0.25
		self.filter_depth = 32
		self.valid_split = 0.8
		self.n = len(self.X_train)

		self.L2reg = 0.005
		self.batch_size = batch_size

		
		self.traingenerator = ImageDataGenerator(featurewise_center=True, featurewise_std_normalization=True, zoom_range = 0.2, rotation_range=45,width_shift_range=0.2,height_shift_range=0.2,horizontal_flip=True,vertical_flip=True)
		self.validategenerator = ImageDataGenerator(featurewise_center=True, featurewise_std_normalization=True)

		self.testgenerator = ImageDataGenerator(featurewise_center=True, featurewise_std_normalization=True)


		self.cut = (int(self.n*self.valid_split))

		self.train_gen = self.traingenerator.flow(self.X_train[0:self.cut],self.y_train[0:self.cut],batch_size=self.batch_size)
		self.valid_gen = self.validategenerator.flow(self.X_train[self.cut:],self.y_train[self.cut:],batch_size=self.batch_size)
		#self.test_gen = self.testgenerator.flow(self.X_test,batch_size=self.batch_size)

		if pretrained_model is not None:
			self.res_CNN_classifier = load_model(pretrained_model)
			self.res_CNN_classifier.name='Pretrained model'
			self.res_CNN_classifier.summary()

		else:
			self.create_res_CNN_classifier()

		

	def train(self,epochs=100,patience=20):

		early_stopping = EarlyStopping(monitor='val_loss', patience=patience)

		log = self.res_CNN_classifier.fit_generator(self.train_gen,
			steps_per_epoch= (self.n*self.valid_split) / self.batch_size,
			validation_data= self.valid_gen,
			validation_steps = (self.n - self.n*self.valid_split) / self.batch_size,
			epochs=epochs)

		print(log.history)

		self.res_CNN_classifier.save('model.h5')

		with open('results.pickle', 'wb') as f:
    			pickle.dump(log.history, f)

		
		#self.prediction = self.res_CNN_classifier.evaluate(self.X_train,self.y_train)

		#print(self.evaluation)

	def make_prediction(self):
		self.prediction = self.res_CNN_classifier.predict_generator(self.test_gen, steps=(self.n / self.batch_size))

		self.submission = pd.DataFrame({'id': self.test_df["id"], 'is_iceberg': self.prediction.reshape((self.prediction.shape[0]))})

		print('testing a sample of the predictions')
		print(self.submission.head(10))

		self.submission.to_csv("./submission.csv", index=False)

	def calculate_statistics(self):
		#self.validation_predictions = self.res_CNN_classifier.predict_generator(self.valid_gen, steps = (self.n - self.n*self.valid_split) / self.batch_size)
		self.validation_predictions = self.res_CNN_classifier.predict(self.X_train[self.cut:])

		

		print(self.validation_predictions[0:10])
		
		labels = self.y_train[self.cut:]
		id_ice = self.train_df["id"]
		id_ice_valid = id_ice[self.cut:]

		metrics = accuracy_score(labels, self.validation_predictions)
		print(metrics)

		self.validation_predictions -= 0.5
		self.

		

		for i in range(len(self.validation_predictions)):
			pred = self.validation_predictions[i]
			label = labels[i]

			print('Pred: %f  Label: %f' % (pred,label))

			if i % 10 == 0 and i != 0:
				i=input()



	def create_res_CNN_classifier(self):

		self.filter_depth = 128

		kernel_encoder = 3

		input_shape = Input(shape=(self.X_train.shape[1],self.X_train.shape[2],self.X_train.shape[3]),name='image')

		#input_angle = Input(shape=[1], name="angle")

		print('input', k.int_shape(input_shape))

		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(input_shape)
		x = BatchNormalization()(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		

		x = Add()([x, temp])

		x = MaxPooling2D(pool_size=(2,2),padding='same')(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)

		x = Add()([x, temp])

		x = MaxPooling2D(pool_size=(2,2),padding='same')(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)

		x = Add()([x, temp])

		x = MaxPooling2D(pool_size=(2,2),padding='same')(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)

		x = Add()([x, temp])

		x = MaxPooling2D(pool_size=(2,2),padding='same')(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)

		x = Add()([x, temp])

		#x = MaxPooling2D(pool_size=(2,2),padding='same')(x)


		x = Flatten()(x)


		x = Dense(100)(x)

		#x = Concatenate()([x,BatchNormalization()(input_angle)])

		x = LeakyReLU(alpha=self.alpha)(x)
		x = Dropout(self.dropout)(x)

		x = Dense(1)(x)
		x = Activation('sigmoid')(x)

		self.res_CNN_classifier = Model(inputs=input_shape, outputs=x)

		self.res_CNN_classifier.summary()

		optAdam = optimizers.Adam(lr=0.00001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=1e-08)
		self.res_CNN_classifier.compile(loss='binary_crossentropy', optimizer=optAdam, metrics=['accuracy'])


	def create_res_CNN_classifier(self):

		self.filter_depth = 128

		kernel_encoder = 3

		input_shape = Input(shape=(self.X_train.shape[1],self.X_train.shape[2],self.X_train.shape[3]),name='image')

		#input_angle = Input(shape=[1], name="angle")

		print('input', k.int_shape(input_shape))

		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(input_shape)
		x = BatchNormalization()(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		

		x = Add()([x, temp])

		x = MaxPooling2D(pool_size=(2,2),padding='same')(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)

		x = Add()([x, temp])

		x = MaxPooling2D(pool_size=(2,2),padding='same')(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)

		x = Add()([x, temp])

		x = MaxPooling2D(pool_size=(2,2),padding='same')(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)

		x = Add()([x, temp])

		x = MaxPooling2D(pool_size=(2,2),padding='same')(x)

		temp = x

		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)
		
		x = BatchNormalization()(x)
		x = LeakyReLU(alpha=self.alpha)(x)
		x = Conv2D(self.filter_depth, kernel_encoder, strides=self.stride, padding='same')(x)

		x = Add()([x, temp])

		#x = MaxPooling2D(pool_size=(2,2),padding='same')(x)


		x = Flatten()(x)


		x = Dense(100)(x)

		#x = Concatenate()([x,BatchNormalization()(input_angle)])

		x = LeakyReLU(alpha=self.alpha)(x)
		x = Dropout(self.dropout)(x)

		x = Dense(1)(x)

		self.res_CNN_classifier = Model(inputs=input_shape, outputs=x)

		self.res_CNN_classifier.summary()

		optAdam = optimizers.Adam(lr=0.00001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=1e-08)
		self.res_CNN_classifier.compile(loss='binary_crossentropy', optimizer=optAdam, metrics=['accuracy'])


	def create_angle_CNN(self):
		self.model = Sequential(name='CNN')

		kernel_encoder = 3

		input_shape = (self.X_train.shape[1],self.X_train.shape[2],self.X_train.shape[3])


		self.model.add(Conv2D(self.filter_depth*1, kernel_encoder, strides=self.stride, input_shape=input_shape, padding='same'))
		self.model.add(LeakyReLU(alpha=self.alpha))
		self.model.add(MaxPooling2D(pool_size=(2,2),padding='same'))
		self.model.add(BatchNormalization())

		self.model.add(Dropout(self.dropout))

		self.model.add(Conv2D(self.filter_depth*2, kernel_encoder, strides=self.stride, input_shape=input_shape, padding='same'))
		self.model.add(LeakyReLU(alpha=self.alpha))
		self.model.add(MaxPooling2D(pool_size=(2,2),padding='same'))
		self.model.add(BatchNormalization())

		self.model.add(Conv2D(self.filter_depth*3, kernel_encoder, strides=self.stride, input_shape=input_shape, padding='same'))
		self.model.add(LeakyReLU(alpha=self.alpha))
		self.model.add(MaxPooling2D(pool_size=(2,2),padding='same'))
		self.model.add(BatchNormalization())

		self.model.add(Dropout(self.dropout))

		self.model.add(Conv2D(self.filter_depth*4, kernel_encoder, strides=self.stride, input_shape=input_shape, padding='same'))
		self.model.add(LeakyReLU(alpha=self.alpha))
		self.model.add(MaxPooling2D(pool_size=(2,2),padding='same'))
		self.model.add(BatchNormalization())

		self.model.add(Dropout(self.dropout))

		self.model.add(Conv2D(self.filter_depth*5, kernel_encoder, strides=self.stride, input_shape=input_shape, padding='same'))
		self.model.add(LeakyReLU(alpha=self.alpha))
		self.model.add(MaxPooling2D(pool_size=(2,2),padding='same'))
		self.model.add(BatchNormalization())

		# Out: 1-dim probability
		self.model.add(Flatten())
		self.model.add(Dropout(self.dropout))
		self.model.add(Dense(20))
		self.model.add(LeakyReLU(alpha=self.alpha))
		
		self.model.add(Dropout(self.dropout))
		self.model.add(Dense(1))
		#self.model.add(Activation('sigmoid'))

		self.model.summary()
		optAdam = optimizers.Adam(lr=0.00001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=1e-07)
		self.model.compile(loss='binary_crossentropy', optimizer=optAdam, metrics=['accuracy'])
		
	def show_sample(self,index):
		class_ = -1
		if self.y_train[index] == 1:
			class_ = 'Iceberg'
		else:
			class_ = 'Boat'


		print(class_, ' angle: ',str(self.X_angle_train[index]))
		fig, (ax1, ax2) = plt.subplots(1,2, figsize = (12, 6))
		#plt.title(class_, ' angle: ',str(self.X_angle_train[index]))
		ax1.matshow(self.X_train[index,:,:,0])
		ax1.set_title('Band 1')
		ax2.matshow(self.X_train[index,:,:,1])
		ax2.set_title('Band 2')
		plt.show()



if __name__ == "__main__":

	os.environ['CUDA_VISIBLE_DEVICES'] = '2'

	pretrained_folder = os.path.join('resNET_nodrop_lr00001','model.h5')

	classifier = CLassifier(batch_size=40, pretrained_model=pretrained_folder)
	#classifier.train(epochs=1500)

	#classifier.make_prediction()
	classifier.calculate_statistics()


