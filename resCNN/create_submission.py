#Load data
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD,Adam
from keras.layers.normalization import BatchNormalization
from keras.applications.xception import Xception
from keras.applications.mobilenet import MobileNet
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from resnet import ResnetBuilder

from sklearn.model_selection import train_test_split


from keras.models import load_model
import matplotlib.pyplot as plt
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '2'

train_df = pd.read_json(os.path.join('..','data','train.json'))
test_df = pd.read_json(os.path.join('..','data','test.json'))

# Load training data
X_band1_train = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in train_df["band_1"]])
X_band2_train = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in train_df["band_2"]])
X_band3_train = (X_band1_train+X_band2_train)/2
X_angle_train = np.array(train_df.inc_angle)
Y_train = np.array(train_df["is_iceberg"])
ID_train = train_df["id"]


# Load testing data
X_band1_test = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in test_df["band_1"]])
X_band2_test = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in test_df["band_2"]])
X_band3_test = (X_band1_test+X_band2_test)/2
X_angle_test = np.array(test_df.inc_angle)


normalize1 = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization = True
)
normalize2 = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization = True
)
normalize3 = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization = True
)

normalize1.fit(X_band1_train)
normalize2.fit(X_band2_train)
normalize3.fit(X_band3_train)

X_band1_train_n = next(normalize1.flow(X_band1_train,None,batch_size=X_band1_train.shape[0],shuffle=False))
X_band2_train_n = next(normalize2.flow(X_band2_train,None,batch_size=X_band2_train.shape[0],shuffle=False))
X_band3_train_n = next(normalize3.flow(X_band3_train,None,batch_size=X_band3_train.shape[0],shuffle=False))
X_train = np.concatenate([X_band1_train_n,X_band2_train_n,X_band3_train_n],axis=-1)


X_band1_test_n = next(normalize1.flow(X_band1_test,None,batch_size=X_band1_test.shape[0],shuffle=False))
X_band2_test_n = next(normalize2.flow(X_band2_test,None,batch_size=X_band2_test.shape[0],shuffle=False))
X_band3_test_n = next(normalize3.flow(X_band3_test,None,batch_size=X_band3_test.shape[0],shuffle=False))
X_test = np.concatenate([X_band1_test_n,X_band2_test_n,X_band3_test_n],axis=-1)

X_train_s, X_valid_s, Y_train_s, Y_valid_s = train_test_split(X_train, Y_train, test_size=0.15, random_state=42)


#Load model
pretrained_path = os.path.join('resnet_100_epochs_96_valacc','checkpoint_resnet.h5')
model = load_model(pretrained_path)
model.summary()

predictions  = model.predict(X_test, verbose=1)

submission = pd.DataFrame({'id': test_df["id"], 'is_iceberg': predictions.reshape((predictions.shape[0]))})

print('testing a sample of the predictions')
print(submission.head(10))

submission.to_csv("./submission.csv", index=False)



certainty_lim = 0.2

black_list06= {}
black_list07= {}
display = True

for i in range(predictions.shape[0]):
    iceberg_pred = predictions[i,1]
    #If uncertain prediction, plot image



   # if iceberg_pred > certainty_lim and iceberg_pred < 1-certainty_lim and display:
    if abs(iceberg_pred - Y_train[i]) > 0.6:
        black_list06[ID_train[i]] = (str(iceberg_pred),str(Y_train[i]),str(abs(iceberg_pred - Y_train[i])))

        if display:
            band_1 =  X_train[i,:,:,0]
            band_2 =  X_train[i,:,:,1]
            band_3 =  X_train[i,:,:,2]

            instance = ''
            if Y_train[i] == 1:
                instance='iceberg'
            else:
                instance='boat'

            fig = plt.figure()
            plt.title('Prediction: ' + str(iceberg_pred) + '| Label: ' + instance + str(Y_train[i]))
            ax_1 = fig.add_subplot(1,3,1)
            img_1 = ax_1.imshow(band_1)
            plt.colorbar(img_1, ax=ax_1)
            ax_2 = fig.add_subplot(1,3,2)
            img_2 = ax_2.imshow(band_2)
            plt.colorbar(img_2, ax=ax_2)
            ax_3 = fig.add_subplot(1,3,3)
            img_3 = ax_3.imshow(band_3)
            plt.colorbar(img_3, ax=ax_3)
            plt.show()


    if abs(iceberg_pred - Y_train[i]) > 0.7:
        black_list07[ID_train[i]] = (str(iceberg_pred),str(Y_train[i]),str(abs(iceberg_pred - Y_train[i])))

print(len(black_list07))
print(len(black_list06))

import json
with open('black_list07.json', 'w') as fp:
    json.dump(black_list06, fp)

with open('black_list06.json', 'w') as fp:
    json.dump(black_list07, fp)
