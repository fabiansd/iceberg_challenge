#Load data
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD,Adam
from keras.layers.normalization import BatchNormalization
from keras.applications.xception import Xception
from keras.applications.mobilenet import MobileNet
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from resnet import ResnetBuilder
from sklearn.model_selection import train_test_split
from keras.models import load_model
import matplotlib.pyplot as plt
import os

# os.environ['CUDA_VISIBLE_DEVICES'] = '2'

train_df = pd.read_json(os.path.join('..','data','train.json'))

# Load training data
X_band1_train = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in train_df["band_1"]])
X_band2_train = np.array([np.array(band).astype(np.float32).reshape(75, 75, 1) for band in train_df["band_2"]])
X_band3_train = (X_band1_train+X_band2_train)/2
X_angle_train = np.array(train_df.inc_angle)
Y_train = np.array(train_df["is_iceberg"])
ID_train = train_df["id"]


normalize1 = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization = True
)
normalize2 = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization = True
)
normalize3 = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization = True
)

normalize1.fit(X_band1_train)
normalize2.fit(X_band2_train)
normalize3.fit(X_band3_train)

X_band1_train_n = next(normalize1.flow(X_band1_train,None,batch_size=X_band1_train.shape[0],shuffle=False))
X_band2_train_n = next(normalize2.flow(X_band2_train,None,batch_size=X_band2_train.shape[0],shuffle=False))
X_band3_train_n = next(normalize3.flow(X_band3_train,None,batch_size=X_band3_train.shape[0],shuffle=False))
X_train = np.concatenate([X_band1_train_n,X_band2_train_n,X_band3_train_n],axis=-1)

X_train_s, X_valid_s, Y_train_s, Y_valid_s, ID_train_s, ID_valid_s = train_test_split(X_train, Y_train, ID_train, test_size=0.15, random_state=42)


#Load model
pretrained_path = './resNET_32_50e_no_angle/saved_resnet_adrian.h5'
model = load_model(pretrained_path)
model.summary()

predictions  = model.predict(X_valid_s, verbose=1)

black_list06= {}
black_list07= {}

for i in range(predictions.shape[0]):
    iceberg_pred = predictions[i]
    if abs(iceberg_pred - Y_valid_s[i]) > 0.6:
        print(type(ID_valid_s))
        black_list06[ID_valid_s.iloc[i]] = (str(iceberg_pred),str(Y_valid_s[i]),str(abs(iceberg_pred - Y_valid_s[i])))

    if abs(iceberg_pred - Y_valid_s[i]) > 0.7:
        black_list07[ID_valid_s.iloc[i]] = (str(iceberg_pred),str(Y_valid_s[i]),str(abs(iceberg_pred - Y_valid_s[i])))

print(len(black_list07))
print(len(black_list06))

import json
with open('black_list07-correct.json', 'w') as fp:
    json.dump(black_list06, fp)

with open('black_list06-correct.json', 'w') as fp:
    json.dump(black_list07, fp)
