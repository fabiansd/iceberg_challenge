import matplotlib.pyplot as plt
import numpy
import pickle
import sys


try:
    historyfile = sys.argv[1]
except IndexError:
    print("Usage: plothistory.py <path/historyfile>")
    sys.exit(1)# start the program

try:
	history = pickle.load( open( historyfile, "rb" ) )
except FileNotFoundError:
	print('Could not find file from the path ' + historyfile)
	sys.exit(1)


# summarize history for loss

print(history)
i=input()

plt.subplot(2,1,1)
plt.plot(history['loss'])
plt.plot(history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['loss', 'val_loss'], loc='upper left')

plt.subplot(2,1,2)
plt.plot(history['acc'])
plt.plot(history['val_acc'])
plt.title('model accuracy')
plt.ylabel('acc')
plt.xlabel('epoch')
plt.legend(['acc', 'val_acc'], loc='upper left')

plt.savefig('Metrics visualized.png',format='png')
plt.show()

